package javabit2cpitester;
import Api.Bit2cClient;
import Enums.CoinType;
import Enums.PairType;
import Objects.*;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Bit2CApiTester {

    public static void main(String[] args) throws Exception {
        Bit2cClient client = new Bit2cClient("https://bit2c.co.il/","eb0798eb-5a23-4765-a9cc-83e41842d603","213DA003C16019DE8DE77EBFB7F52D244E11CECB3DC2A15CDC41F546109DACBD");
        
        //Get User Balance Example
        //UserBalance b = client.Balance();
        /* Get Trades Example */
        //ArrayList<ExchangesTrade> trades = client.GetTrades(PairType.BtcNis, Long.MIN_VALUE, Double.NaN);
        /* Get Ticker Example */
        //Ticker ticker = client.GetTicker(PairType.BtcNis);
        //System.out.println(ticker.a);
        /* Get OrderBook Example */
        //OrderBook orderBook = client.GetOrderBook(PairType.BtcNis);
        
        // Add Order Example
        /*OrderData data = new OrderData();
        data.Amount = BigDecimal.valueOf(0.00001);
        data.IsBid = false;
        data.Pair = PairType.BtcNis;
        data.Price = BigDecimal.valueOf(123);
        data.Total = data.Amount.multiply(data.Price);
        AddOrderResponse addOrder = client.AddOrder(data);*/
        
        //Add Market Order Example
        //client.AddMarketOrder(true, client.Balance().AVAILABLE_NIS, PairType.BtcNis);
        
        /* Get All My Orders */
        //Orders order = client.MyOrders(PairType.BtcNis);
        
        /*Get Account History*/
        //ArrayList<AccountRaw> accountRaws = client.AccountHistory(null, null);
        /* Cancel Order Example 
        CheckoutResponse response = client.CancelOrder(123456);
        */
        
        /* Clear All My Orders 
        //client.ClearMyOrders(PairType.BtcNis);
        */
        /* Create a Checkout Request Example*/
        /*CheckoutLinkModel data = new CheckoutLinkModel();
        data.CancelURL = "";
        data.ReturnURL = "";
        data.CoinType = CoinType.BTC;
        data.Description = "hihihihi";
        data.NotifyByEmail = false;
        data.Price = BigDecimal.valueOf(3424);
        data.Autosale = 0;
        CheckoutResponse response = client.CreateCheckout(data);*/
        
        //Withdraw BTC
        client.Withdraw(CoinType.BTC, 0.003, "3KZeQcmULzUTVKcXXaCvQsKdNN3MbUCn1y");
    }
    
}

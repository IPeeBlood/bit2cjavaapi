/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objects;

import Enums.OrderStatusType;
import Enums.PairType;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author AMiT and Ilan
 */
public class TradeOrder {
    public long created;
    public int type;
    public long id;
    public int order_type;
    public BigDecimal amount;
    public BigDecimal price;
    public BigDecimal stop;
    public PairType pair;
}

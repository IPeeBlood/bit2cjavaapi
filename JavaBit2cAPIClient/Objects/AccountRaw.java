/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objects;

import java.math.BigDecimal;

/**
 *
 * @author AMiT and Ilan
 */
public class AccountRaw {
	public String firstCoin;
	public String secondCoin;
    public BigDecimal firstAmount;
    public BigDecimal secondAmount;
    public BigDecimal feeAmount;
    public String created;
    public BigDecimal fee;
    public String feeCoin;
    public long id;
    public String OrderCreated;
    public BigDecimal price;
    public String Ref;
    public int accountAction;
}

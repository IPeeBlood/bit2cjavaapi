/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objects;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;

/**
 *
 * @author AMiT and Ilan
 */
public class UserBalance {
    public BigDecimal AVAILABLE_BTC;
    public BigDecimal AVAILABLE_LTC;
    public BigDecimal AVAILABLE_BCH;
    public BigDecimal AVAILABLE_NIS;
}


package Api;

import Enums.*;
import Objects.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class Bit2cClient {

    private final String Key;
    private final String Secret;
    private final String Url;
    private long nonce;

    public Bit2cClient(String Url, String Key, String Secret) throws Exception {
        this.Key = Key;
        this.Secret = Secret.toUpperCase();
        this.Url = Url;
        this.nonce = (new Date()).getTime();
    }

    private String ComputeHash(String message) throws Exception {
        Mac mac = Mac.getInstance("HmacSHA512");
        SecretKeySpec secret_spec = new SecretKeySpec(this.Secret.getBytes(), "HmacSHA512");
        mac.init(secret_spec);
        String signature = Base64.getEncoder().encodeToString(mac.doFinal(message.getBytes())).replaceAll("\r\n", "");
        return signature;
    }

    private String Query(HashMap<String, String> data, String Url, String method) {
    	String response = "";
        String line = null;
        BufferedReader reader = null;
        try {
            // add nonce and build arg lis
            String post_data = buildQueryString(data);
            
             if (method.equals("GET")) {
                   Url += "?" + post_data;
            }
            URL queryUrl = new URL(Url);
            // create connection
            HttpURLConnection connection = (HttpURLConnection) queryUrl.openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            connection.setRequestMethod(method);
            connection.setRequestProperty("Timeout", "15000");
            connection.setRequestProperty( "charset", "utf-8");
            /**/
            if (method.equals("POST"))
            {
            	connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            	connection.setRequestProperty("Content-Length", String.valueOf(post_data.length()));
            }
            
            connection.setRequestProperty("Key", this.Key);
            connection.setRequestProperty("Sign", this.ComputeHash(post_data));
        	connection.setDoOutput(true);
        	
            // write post
            if (method.equals("POST"))
            {
            	//
            	//connection.getOutputStream().write(post_data.getBytes());
            	//connection.getOutputStream().close();
            	//
                connection.getOutputStream().write(post_data.getBytes("UTF-8"));
                connection.getOutputStream().close();
            }
            // read info
        	
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
            

            while ((line = reader.readLine()) != null) {
                response = response + line;
            }
            reader.close();
            return response;
            
        } catch (Exception e) {
        	System.out.println(e.toString());
        }
        return null;
    }

    private String buildQueryString(HashMap<String, String> args) {
        String result = new String();
        for (String hashkey : args.keySet()) {
            if (result.length() > 0) {
                result += '&';
            }
            try {
                result += URLEncoder.encode(hashkey, "UTF-8") + "="
                        + URLEncoder.encode(args.get(hashkey), "UTF-8");
            } catch (Exception ex) {
            }
        }
        if (result.isEmpty()) {
            result = "nonce=" + this.nonce;
        } else {
            result += "&nonce=" + this.nonce;
        }
        this.nonce++;
        return result;
    }

    public UserBalance Balance() {
        String url = this.Url + "Account/Balance";
        String data = Query(new HashMap<String, String>(), url,"GET");

        Gson gson = new Gson();
        UserBalance balance = gson.fromJson(data, UserBalance.class);

        return balance;
    }

    private String DownloadString(String Url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Url)).openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            connection.getContent();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            StringBuilder response = new StringBuilder();

            String inputStr = "";
            while ((inputStr = streamReader.readLine()) != null) {
                response.append(inputStr);
            }
            return response.toString();
        } catch (Exception ex) {
        	System.out.println(ex.toString());
        }
        return null;
    }
    
    
    public ArrayList<ExchangesTrade> GetTrades(PairType Pair, Long since, Double date) {
        ArrayList<ExchangesTrade> result = new ArrayList<ExchangesTrade>();
        try {
            String Url = this.Url + "Exchanges/" + Pair.name() + "/trades.json";
            String response = DownloadString(Url);
            Gson gson = new Gson();
            ExchangesTrade[] trades = gson.fromJson(response.toString(), ExchangesTrade[].class);
            result.addAll(Arrays.asList(trades));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public Ticker GetTicker(PairType Pair) {
        try {
            String Url = this.Url + "Exchanges/" + Pair.name() + "/Ticker.json";
            String response = DownloadString(Url);
            Gson gson = new Gson();
            Ticker fromJSON = gson.fromJson(response, Ticker.class);
            return fromJSON;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public OrderBook GetOrderBook(PairType Pair) {
        OrderBook orderBook = null;
        try {
            String Url = this.Url + "Exchanges/" + Pair.name() + "/orderbook.json";
            String response = DownloadString(Url);
            Gson gson = new Gson();
            orderBook = gson.fromJson(response, OrderBook.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderBook;
    }
    
public AddOrderResponse AddMarketOrder(boolean isBid,BigDecimal total, PairType pair) {
    	String URL;
    	String response;
    	HashMap<String, String> params = new HashMap<String, String>();
    	if(isBid)
    	{
    		URL = this.Url + "Order/AddOrderMarketPriceBuy";
    		params.put("Total", String.valueOf(Double.parseDouble(String.valueOf(total))*0.995));
    		params.put("Pair", String.valueOf(pair));
    		
    	}
    	else {
    		URL = this.Url + "Order/AddOrderMarketPriceSell";
    		params.put("Amount", String.valueOf(total));
    		params.put("Pair", String.valueOf(pair));
    	}
    	response = Query(params, URL, "POST");
        Gson gson = new Gson();
        AddOrderResponse addOrderResponse = gson.fromJson(response, AddOrderResponse.class);
        return addOrderResponse;
    }
    public AddOrderResponse AddOrder(OrderData data) {
    	
        String Url = this.Url + "Order/AddOrder";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("Amount", String.valueOf(data.Amount.toPlainString()));
        params.put("IsBid", String.valueOf(data.IsBid));
        params.put("Pair", String.valueOf(data.Pair));
        params.put("Price", String.valueOf(data.Price.toPlainString()));
        params.put("Total", String.valueOf(data.Total.toPlainString()));
        String response = Query(params, Url, "POST");

        Gson gson = new Gson();
        AddOrderResponse addOrderResponse = gson.fromJson(response, AddOrderResponse.class);
        return addOrderResponse;
    }

    public Orders MyOrders(PairType pair) {
        String Url = this.Url + "Order/MyOrders";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("pair", String.valueOf(pair));
        String response = Query(params, Url, "GET");

        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject rootObj = parser.parse(response).getAsJsonObject();    
        String element = rootObj.getAsJsonObject(pair.toString()).toString();
        Orders orders = gson.fromJson(element, Orders.class);
        if(orders.bid != null)
        for(TradeOrder bid : orders.bid)
        {
        	bid.pair = pair;
        }
        if(orders.ask != null)
        for(TradeOrder ask : orders.ask)
        {
        	ask.pair = pair;
        }
        return orders;
    }

    public ArrayList<AccountRaw> AccountHistory(Date fromTime, Date toTime) {
        String Url = this.Url + "Order/AccountHistory";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fromTime", String.valueOf(fromTime == null ? 0 : fromTime));
        params.put("toTime", String.valueOf(toTime == null ? 0 : toTime));
        String response = Query(params, Url, "GET");
        ArrayList<AccountRaw> result = new ArrayList<AccountRaw>();
        Gson gson = new Gson();
        AccountRaw[] accountRaws = gson.fromJson(response, AccountRaw[].class);
        result.addAll(Arrays.asList(accountRaws));
        return result;
    }
    
    public void ClearMyOrders(PairType Pair){
        Orders orders = MyOrders(Pair);
        if(orders.bid != null)
        for(TradeOrder bid : orders.bid)
        {
            if(bid.pair == Pair)
                CancelOrder(bid.id);
        }
        if(orders.ask != null)
        for(TradeOrder ask : orders.ask)
        {
            if(ask.pair == Pair)
                CancelOrder(ask.id);
        }
    }

    public CheckoutResponse CancelOrder(long id) {
        String Url = this.Url + "Order/CancelOrder";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(id));
        String response = Query(params, Url, "POST");

        Gson gson = new Gson();
        CheckoutResponse checkoutResponse = gson.fromJson(response, CheckoutResponse.class);
        return checkoutResponse;
    }

    public CheckoutResponse CreateCheckout(CheckoutLinkModel data) {
        String Url = this.Url + "Merchant/CreateCheckout";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("Price", String.valueOf(data.Price.toPlainString()));
        params.put("Description", String.valueOf(data.Description));
        params.put("CoinType", String.valueOf(data.CoinType.name()));
        params.put("ReturnURL", String.valueOf(data.ReturnURL));
        params.put("CancelURL", String.valueOf(data.CancelURL));
        params.put("NotifyByEmail", String.valueOf(data.NotifyByEmail));
        params.put("Autosale", String.valueOf(data.Autosale));
        String response = Query(params, Url, "POST");

        Gson gson = new Gson();
        CheckoutResponse checkoutResponse = gson.fromJson(response, CheckoutResponse.class);
        return checkoutResponse;
    }
    
    public String Withdraw(CoinType coin, double amount, String address) {
    	String Url = this.Url + "Order/WithdrawCoin";
    	HashMap<String, String> params = new HashMap<String, String>();
    	params.put("Total", String.valueOf(amount));
    	params.put("Address", address);
    	params.put("Coin", coin.toString()); 
    	params.put("subtractFeeAmount", String.valueOf(false));
    	//params.put("fee", "0.1");
    	//params.put("gasLimit", "0.1");
    	//params.put("gasPrice", "0.1");
    	String response = Query(params, Url, "POST");
    	return response;
    }
}
